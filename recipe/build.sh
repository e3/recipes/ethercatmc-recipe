#!/bin/bash

LIBVERSION=${PKG_VERSION}

# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db_internal
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install
